import { CodeBucketError } from '../error';
import { CommandBase } from './command-base';

export class OpenInIssueTrackerCommand extends CommandBase {

  protected async execute(): Promise<void> {

    const backend = await this.getBackend();
    const remote = await backend.findRemoteHost();
    const filePath = this.getFilePath(backend.root);
    const rev = await backend.findSelectedRevision(filePath, this.getCurrentLine());
    const message = await backend.getRevisionMessage(rev);

    const issueTrackers = await remote.getIssueTrackers();
    const issueUrls = issueTrackers.map(tracker => tracker.findIssueUrl(message)).filter(isDefined);

    if (issueUrls.length === 0) {
      throw new CodeBucketError('Unable to find any matching issue keys.');
    }

    this.openUrl(issueUrls[0]);
  }

}

function isDefined(str: string | undefined): str is string {
  return str !== undefined && str.length > 0;
}
