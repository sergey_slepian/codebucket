import * as path from 'path';
import { IssueTracker } from '../issuetracker/issuetracker';
import { Host, HostConfig } from './host-base';

export class BitbucketHost extends Host implements IssueTracker {

  constructor(cfg: HostConfig) {
    super(cfg);
  }

  public getChangeSetUrl(revision: string, filePath: string): string {
    return `${this.webHost}/${this.repo}/commits/${revision}#chg-${encodeURIComponent(filePath)}`;
  }

  public getSourceUrl(revision: string, filePath: string, lineRanges: string[]) {
    const ranges = lineRanges.join(',');
    const hash = `${encodeURIComponent(path.basename(filePath))}-${ranges}`;
    return `${this.webHost}/${this.repo}/src/${revision}/${encodeURIComponent(filePath)}#${hash}`;
  }

  public getPullRequestUrl(id: number, filePath: string): string {
    return `${this.webHost}/${this.repo}/pull-requests/${id}/diff#chg-${encodeURIComponent(filePath)}`;
  }

  public async getIssueTrackers(): Promise<IssueTracker[]> {
    const trackers = await super.getIssueTrackers();
    trackers.push(this);
    return trackers;
  }

  public findIssueUrl(message: string): string | undefined {
    const match = message.match(/\B\#(\d+)\b/);
    if (match) {
      return `${this.webHost}/${this.repo}/issues/${match[1]}`;
    }
  }

}
